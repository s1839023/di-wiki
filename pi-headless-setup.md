# Set up Headless Pi on eduroam
These instructions allow you to connect your Raspberry Pi to eduroam, and use it in 'headless' mode, whereby the Pi doesn't require a screen or keyboard because you control it remotely via ssh. *Note: you will require a screen and keyboard initially to configure the Pi.*

Adapted from https://desertbot.io/blog/headless-raspberry-pi-3-bplus-ssh-wifi-setup

### Download Raspbian
[Download Raspbian lite](https://www.raspberrypi.org/downloads/raspbian/) and install on a micro SD card using [Etcher](https://www.balena.io/etcher/).

### Enable SSH
1. Run Notepad or any text editor
2. In a new file put in one space and nothing more
3. Click File / Save As ...
4. Be sure to set Save as type to All Files (so the file is NOT saved with a .txt extension)
5. Call the file ssh and save it onto the SD card containing Raspbian (should be named `boot`).
6. Close the file

### Enable eduroam
1. Using the same method as above, create a file on the SD card and name it `wpa_supplicant.conf`.
2. Add the following text to the wpa_supplicant file, replacing the text inside square brackets with your credentials: **Note: it is not advisable to use a plain-text version of your password. Instead you can hash the password using the following command: `echo -n plaintext_password_here | iconv -t utf16le | openssl md4` or on MacOS: `echo -n plaintext_password_here | iconv -t UTF-16LE | openssl md4`. Then insert the output (password_hashed) of this command into the password field above as `password=hash:password_hashed`**

```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=GB  
network={
        ssid="eduroam"
        proto=RSN
        key_mgmt=WPA-EAP
        auth_alg=OPEN
        eap=PEAP
        identity="[your UUN@ed.ac.uk]"
        anonymous_identity="[your UUN@ed.ac.uk]"
        password=[your EASE password]
        ca_cert="/etc/ssl/certs/uoe-net-ca.pem"
        phase1="peaplabel=0"
        phase2="auth=MSCHAPV2"
        priority=999
        proactive_key_caching=1
}
```

3. Download [this certificate file](/uoe-net-ca.pem) and add it to the SD card.

### Boot the Pi
1. Insert the SD card into the Pi and boot it with a screen and keyboard connected.
2. Log in to the Pi with the default username (pi) and password (raspberry).
3. Use the following command to move the eduroam certificate file into the correct directory: `sudo mv /boot/uoe-net-ca.pem /etc/ssl/certs/`.
4. Reboot the Pi: `sudo reboot`.
5. When the Pi has rebooted, log in again and type `ifconfig`. You should see an IP address in the wlan0 section, after inet. For example `inet 172.20.169.72 `. Make a note of the IP address.

### Login via SSH
1. On a separate computer connected to eduroam, use the following command to connect to your Pi: `ssh pi@[the IP address you copied above]`. For example: `ssh pi@172.20.169.72`.
2. If you are prompted with a message similar to `Are you sure you want to continue connecting (yes/no)?`, then type `yes` to continue.
3. When prompted for a password, log in with the default pi password (raspberry).
