# Welcome, Design Informatics coder!

In Design Informatics (DI) we create all kinds of weird and wonderful things using code - a BitCoin coffee machine, geo-located smart-boxes, data visualisation websites, trust balls....

For the most part, we want our creations to be **seen**, **experienced**, **used**, and maybe even **adapted** by others. To achieve this, we aim to create code that is:

- **Runnable** - are there sufficient instructions and resources available for us to run and deploy the code?
- **Readable** - can we understand what the code does, so that it can be adapted or improved?

We do our best to provide resources to help you create runnable, readable code. This WIKI is your first port of call to discover the resources that are available to you.

## Let's start with git...
**All** code relating to DI projects should be managed using the git version control system and stored remotely in the [DI GitLab group](https://git.ecdf.ed.ac.uk/design-informatics/).

> **Why?** Because git allows us to keep track of changes to our code, and provides some great tools to help us collaboratively manage and document the development process.

If you're unfamiliar with git then head [here](/AboutGit.md) for resources and tutorials to get up to speed. You can also contact [Evan](mailto:e.morgan@ed.ac.uk) for a tutorial.

#### Step 1: Get a GitLab project
If a GitLab project or sub-group doesn't already exist for the project you're working on, email [Evan](mailto:e.morgan@ed.ac.uk).

#### Step 2: Read the git guidelines
We have a few pointers on best practice when it comes to using git. Read them [here]() (TODO).
