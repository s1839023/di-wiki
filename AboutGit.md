# About Git

Git is a *version control system* - it allows us to keep track of different versions of our code as we develop it. The section below introduces key concepts (with examples) that you will need to be familiar with if you are using git.

## Key Concepts
<dl>
  <dt>Repository</dt>
  <dd>This is a folder where git stores all of the information required to track changes to your project. The repository folder is always named <code>.git</code>.
    <details>
        <summary><em>Example</em></summary>
        You have a folder <code>AwesomeProject</code> that contains all of the files relating to your project. When you set up git for this project, it will create a <b>repository</b> inside the <code>AwesomeProject</code> folder.
    </details>
  </dd>
</dl>

<dl>
  <dt>Commit</dt>
  <dd>This is a snapshot of your project at a particular point in time. Git saves all of your commits with unique IDs, which means you can easily restore your project to a previous version. Each commit should also include a message, which describes the changes you are committing.
    <details>
        <summary><em>Example</em></summary>
        After implementing a new location detection feature in your code, you save the code and then commit it with the message "added new feature to detect location".
    </details>
  </dd>
</dl>

<dl>
  <dt>Branch</dt>
  <dd>This is a separate version of you project that you can work on, creating new commits without changing the original branch of your project. The original branch is usually called <code>master</code>.
    <details>
        <summary><em>Example</em></summary>
        You've developed a website and you want to store the live version of the site, whilst working on a future redesign. You create a branch called <code>redesign</code> and start working on the redesign in that branch, whilst the live site remains unchanged in the <code>master</code> branch.
    </details>
  </dd>
</dl>

<dl>
  <dt>Merge</dt>
  <dd>This is where you integrate (merge) a branch of your project into the original master branch. Git tries to integrate the changes automatically, but this isn't always possible and might result in <em>conflicts</em>, which you have to resolve manually.
    <details>
        <summary><em>Example</em></summary>
        You've made changes in a development branch of your project and you're happy with the changes so you decide to merge these changes back into the original master branch.
    </details>
  </dd>
</dl>

<dl>
  <dt>Push/Pull</dt>
  <dd>When you upload your local git changes to a remote version of your repository then you <b>push</b> them. When you download changes from the remote repository, you <b>pull</b> them.
    <details>
        <summary><em>Example</em></summary>
        You're collaborating with a colleague to write some code, and you have a remote version of the repository set up on GitLab. You make some changes to the code on your local machine and, after committing these changes, you push them to the remote repository. The next time your colleague goes to work on the code, they start by pulling the latest version of the repository from GitLab so that they have the up-to-date version including the changes you made.
    </details>
  </dd>
</dl>

<dl>
  <dt>Clone</dt>
  <dd>This is when you download a copy of a project from a remote repository.
    <details>
        <summary><em>Example</em></summary>
        You are asked to do some work on an existing project, which has a repository set up on GitLab. You start by cloning the project onto your own computer so that you can work on the code locally.
    </details>
  </dd>
</dl>

## Set up
These are the initial steps you should follow if you intend to use GitLab in Design Informatics.


1. **Install git**: before using git, you need to make sure you have it installed on your computer. Follow instructions [here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) to install git. *Note: if you are using windows then we recommend installing [git for windows](https://gitforwindows.org/)*
2. **Add an SSH key**: SSH keys allow you to establish a secure connection between your computer and GitLab (so that you can push and pull without entering your credentials each time). Follow instructions [here](https://git.ecdf.ed.ac.uk/help/ssh/README#generating-a-new-ssh-key-pair) to generate an SSH key on your computer, then [add the SSH key to your account](https://git.ecdf.ed.ac.uk/profile/keys).
3. **Clone a GitLab project**: go to the GitLab page for the project you want to work on and click the blue 'clone' dropdown in the top right corner. Copy the text in the 'Clone with SSH' field. Now open a terminal on your computer. Go to the directory where you want clone you project (e.g. `cd /Users/evan/GitLab/`). Now clone your project into this directory using the command `git clone` followed by the SSH text that you just copied. For example, to clone this wiki repository you would use the command `git clone git@git.ecdf.ed.ac.uk:design-informatics/di-wiki.git`.

## Basic workflows
Here are some examples of basic git workflows. **All of the `git` commands should be executed inside your GitLab project folder, or any of its sub-folders.**

### Get the latest version of your project
1. There is a chance that the local version of your project is out of date, because one of your collaborators has made some changes and pushed them to GitLab.
2. Pull the latest version from GitLab: `git pull`.

### Adding new folders and files
1. You've added a new folder called `data` to your project. The folder contains data files.
2. Tell git to track this new folder and its contents: `git add data`. *Note: if you've added multiple files and folders and you want git to track them all then you can use the command `git add .`*
3. Commit these changes locally, and include a message: `git commit -m "added data files"`
4. Push your changes to GitLab: `git push`.

### Modifying existing files
1. You've added some code to an existing file in your project.
2. Add and commit your changes locally, with a message describing them: `git commit -a -m "added code to load data files"`.
3. Push your changes to GitLab: `git push`.

### Working on a different branch
1. You've [created a new branch](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-new-branch-from-a-projects-dashboard) called `develop` and would like to work on this branch while you test some new code.
2. Switch to the branch using the command `git checkout develop`.
